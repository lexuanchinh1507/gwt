package com.lxchinh.hello.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RpcRequestBuilder;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.*;
import com.gwt.keycloak.Keycloak;
import com.gwt.keycloak.KeycloakListenerAdapter;
import com.lxchinh.hello.shared.FieldVerifier;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Hello implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";
	private static final String UNAUTHORIZED_STATUS = "401";
	private static final int MIN_TOKEN_VALIDITY_SECONDS = -1;

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	private final String configURL = "http://localhost:8888/Hello/keycloak.json";
	Keycloak keycloak = new Keycloak( "application", configURL);
	public void onModuleLoad() {

		keycloak.init();
		keycloak.addKeycloakListener(new KeycloakListenerAdapter() {
			@Override
			public void onReady(Keycloak keycloak, boolean isAuthenticated) {
				if (isAuthenticated) {
					loadUI();
				} else {
					keycloak.login();
				}
			}

			@Override
			public void onAuthSuccess(Keycloak keycloak) {
				Label label = new Label("onAuthSuccess");
				RootPanel.get().add(label);
			}

			@Override
			public void onAuthError(Keycloak keycloak) {
				Label label = new Label("onAuthError");
				RootPanel.get().add(label);
			}

			@Override
			public void onAuthRefreshSuccess(Keycloak keycloak) {
				Label label = new Label("onAuthRefreshSuccess");
				RootPanel.get().add(label);
			}

			@Override
			public void onAuthRefreshError(Keycloak keycloak) {
				Label label = new Label("onAuthRefreshError");
				RootPanel.get().add(label);
			}

			@Override
			public void beforeAuthLogout(Keycloak keycloak) {
				Label label = new Label("beforeAuthLogout");
				RootPanel.get().add(label);
			}

			@Override
			public void onAuthLogout(Keycloak keycloak) {
				Label label = new Label("onAuthLogout");
				RootPanel.get().add(label);
			}

			@Override
			public void onTokenExpired(Keycloak keycloak) {
				Label label = new Label("onTokenExpired");
				RootPanel.get().add(label);
				keycloak.updateToken(MIN_TOKEN_VALIDITY_SECONDS, new Runnable() {
					@Override
					public void run() {
						Label label = new Label("Update token");
						RootPanel.get().add(label);
					}
				});
			}
		});
	}

	private void addTokenToHeader(final String token) {
		((ServiceDefTarget) greetingService).setRpcRequestBuilder(new RpcRequestBuilder() {

		    @Override
		    protected void doFinish(RequestBuilder rb) {
		    	// TODO Auto-generated method stub
		    	super.doFinish(rb);
		    	rb.setHeader("token", token);
		    }
		});
	}

	private void loadUI() {

		Label label = new Label("Hello: " + keycloak.getUsername());
		RootPanel.get().add(label);

		final Button logout = new Button("Logout");
		RootPanel.get().add(logout);
		logout.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				keycloak.logout();
			}
		});

		final Button sendButton = new Button("Send");
		final TextBox nameField = new TextBox();
		nameField.setText("GWT User");
		final Label errorLabel = new Label();

		// We can add style names to widgets
		sendButton.addStyleName("sendButton");

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("nameFieldContainer").add(nameField);
		RootPanel.get("sendButtonContainer").add(sendButton);
		RootPanel.get("errorLabelContainer").add(errorLabel);

		// Focus the cursor on the name field when the app loads
		nameField.setFocus(true);
		nameField.selectAll();

		// Create the popup dialog box
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Remote Procedure Call");
		dialogBox.setAnimationEnabled(true);
		final Button closeButton = new Button("Close");
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId("closeButton");
		final Label textToServerLabel = new Label();
		final HTML serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
		dialogVPanel.add(textToServerLabel);
		dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
		dialogVPanel.add(serverResponseLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);

		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
				sendButton.setFocus(true);
			}
		});

		// Create a handler for the sendButton and nameField
		class MyHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				handleEvent();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					handleEvent();
				}
			}

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void handleEvent() {
				// First, we validate the input.
				errorLabel.setText("");
				final String textToServer = nameField.getText();
				if (!FieldVerifier.isValidName(textToServer)) {
					errorLabel.setText("Please enter at least four characters");
					return;
				}

				// Then, we send the input to the server.
				sendButton.setEnabled(false);
				textToServerLabel.setText(textToServer);
				serverResponseLabel.setText("");
				if (keycloak.isTokenExpired()) {
					keycloak.updateToken(-1, new Runnable() {
						@Override
						public void run() {
							sendNameToServer(keycloak.getToken(), textToServer);
						}
					});
				} else {
					sendNameToServer(keycloak.getToken(), textToServer);
				}
			}

			private void sendNameToServer(String token, String textToServer) {
				addTokenToHeader(token);

				greetingService.greetServer(textToServer, new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user
						dialogBox.setText("Remote Procedure Call - Failure");
						serverResponseLabel.addStyleName("serverResponseLabelError");
						serverResponseLabel.setHTML(SERVER_ERROR);
						dialogBox.center();
						closeButton.setFocus(true);
					}

					public void onSuccess(String result) {
						if (result.equals(UNAUTHORIZED_STATUS)) {
							keycloak.logout();
						}
						dialogBox.setText("Remote Procedure Call");
						serverResponseLabel.removeStyleName("serverResponseLabelError");
						serverResponseLabel.setHTML(result);
						dialogBox.center();
						closeButton.setFocus(true);
					}
				});

			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		sendButton.addClickHandler(handler);
		nameField.addKeyUpHandler(handler);
	}
}
